#include <iostream>
#include <cmath>
using namespace std;
// Write a program that prompts the user for the radius of a circle,
// then calls a function circleArea to calculate the area of that circle.

double circleArea(double radius){
    return 3.142 * pow(radius,2);
}

int main() {
    double radius;
    cout<<"Enter the radius of your circle ";
    cin>>radius;
    double theArea = circleArea(radius);
    cout<<"The area of your circle is  "<<theArea;
    return 0;
}
