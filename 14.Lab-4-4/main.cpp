#include <iostream>
using namespace  std;

int main() {
    /*
     * Write a simple program that allows a user to
     * compute the multiplication of two decimal point numbers (floating point numbers)
     * */
    float firstDecimal;
    float secondDecimal;
    float result;

    cout<<"What is your first decimal? ";
    cin>>firstDecimal;

    cout<<"What is your second decimal? ";
    cin>>secondDecimal;

    result = firstDecimal * secondDecimal;

    cout<<"The result of your calculation is "<<result;

    return 0;
}
