#include <iostream>
using namespace std;

// do while loop to display numbers 73-415
// in do while, the condition comes last.
// increment or decrement must be inside the curly braces for do{}
//

int main() {
    int x = 73;
    int sum = 0;

    do {
        cout<<x<<" ";
        sum += x;
        x++;
    } while(x <= 415);
    cout<<sum;
    return 0;
}
