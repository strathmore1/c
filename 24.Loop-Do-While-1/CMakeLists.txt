cmake_minimum_required(VERSION 3.26)
project(24_Loop_Do_While_1)

set(CMAKE_CXX_STANDARD 17)

add_executable(24_Loop_Do_While_1 main.cpp)
