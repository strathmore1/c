#include <iostream>
using namespace std;

void printNumber(int* numberPointer){
    cout<<*numberPointer<<"\n";
}

void printChar(char* charPtr){
    cout<<*charPtr<<"\n";
}

void print(void* ptr,char type){
    switch(type){
        case 'i':
            *((int*)ptr);
            cout<<*((int*)ptr);
            break;
            // casting to int
        case 'c':
            *((char*)ptr);
            cout<<*((char*)ptr);
            break;
        default:
            cout<<"invalid";
    }
}

int main() {
    // VOID POINTER
    // Can hold pointers to any type of variables e.g float, int, bool e.t.c
    // cannot directly de-reference a void pointer

    // NB: If a function is using the arguments of pointer, the parameter must be a pointer too
    // int* myNumber  will take &myNumber as parameter
    int number = 5;
    printNumber(&number);

    char letter = 'A';
    printChar(&letter);
    print(&number,'i');
    print(&letter,'c');

    return 0;
}


