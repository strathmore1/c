#include <iostream>
using namespace std;

int main() {

    /*
     * Fundamental Data Types
     * 1. int -> size 4bytes. can be short and long int
     * 2. bool -> size 1byte - true/false
     * 3. Float -> 4 bytes - floating point values. 7 digit range
     * 4. Double -> 8 bytes - double precision. 15 digits
     * 5. char -> characters. Single character like 'B','W' etc
     * 6. String -> combination of characters a word. String name = "Bix"
     * */

    float len,wid, ar;
    len = 8.7;
    wid = 5.5;

    ar = len * wid;

    cout << "The area of the rectangle is " << ar << " cm^2 \n";

    /*
     * Operators
     * There are three types of operators in programming
     * 1. Arithmetic operators +, - , /, *, % . They are binary operators
     * 2. Relational and equality operators .
     * 3. Logical operators. AND, OR.
     * */

    float firstNumber = 0.03;
    float secondNumber = 1.1;
    float answer = secondNumber - firstNumber;
    cout << "The answer is  " << answer ;
    return 0;
}
