#include <iostream>
using namespace std;

int main() {
    // Variable -> a container that stores certain values
    // Pointers -> are containers as well which store address values of variables

    int n = 5;
    cout << n <<endl;

    // n address
    cout << &n <<endl; // returns an address of 0x7ffe4499a2f4

    // Creating a pointer
    int* ptr = &n; // ptr is holding the address of the n variable
    cout <<ptr<<endl; // returns the same value as the &n address 0x7ffe4499a2f4

    string myName = "Max";
    // myName address is &n
    cout<<&myName<<"\n"; // Address is 0x7ffcd698eba0
    // myName pointer is string* myNamePtr
    string* myNamePtr = &myName;
    cout<<myNamePtr<<endl; // The pointer is 0x7ffcd698eba0

    // Accessing the value by de-referencing
    // This will get you the value that is stored in the address(pointer)
    cout<<*myNamePtr<<"\n";
    cout<<*ptr<<"\n";

    // Changing the value in a pointer/address
    *myNamePtr = "James Bond";
    *ptr = 20;

    cout<<*myNamePtr<<"\n"; // Value will be changed to James Bond
    cout<<*ptr<<"\n"; // Value will be changed to 20

    //NB: Pointer has to be of the same type as the type of variable it is pointing to
    // int* numberPtr -> int number;
    // string* namePtr -> string name;

    // Creating a pointer, de-referencing and assigning it a value
    // NB: You cannot create a pointer without a variable to assign memory address.
    int value;
    int* ptrTwo = &value; // Creating a pointer
    // int* ptrTwo means a pointer to an integer

    *ptrTwo = 54; // De-referencing the pointer and assigning it value

    cout<<"The value = "<<value;

    // USES OF POINTERS
    // 1. Using pointers to pass values to a function by reference.
    // 2. Using pointers to return multiple values from a function
    // 3. For dynamic memory allocation
    // 4. On oop

    return 0;
}
