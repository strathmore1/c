cmake_minimum_required(VERSION 3.26)
project(27_Pointers)

set(CMAKE_CXX_STANDARD 17)

add_executable(27_Pointers main.cpp)
