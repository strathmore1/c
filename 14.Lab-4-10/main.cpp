#include <iostream>
using namespace std;

int main() {
    /*
     * Write a program that prompts a user to input the following information
        i. How old are you?
        ii. What is your dream job?
        iii. How much salary would you like to be payed?
        iv. What is your dream holiday destination?
     * */

    int age;
    string dreamJob;
    int salary;
    string dreamDestination;

    cout<<"What is your age ";
    cin>>age;
    cout<<"What is your dream job? ";
    cin>>dreamJob;
    cout<<"How much salary would you like to be paid? ";
    cin>>salary;
    cout<<"What is your dream holiday destination? ";
    getline(cin,dreamDestination);
    cout<<"Thank you for this information ";
    return 0;
}
