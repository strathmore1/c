#include <iostream>
using namespace std;

int main() {
    cout << "///////////////////\\\\\\\\\\\\\\\\\\\n";
    cout << "==\tStudents Points\t\t==\n";
    cout << "\\\\\\\\\\\\\\\\\\\/////////////////\n";
    cout << "Name\t\tLab\t\tBonus\tTotal\n";
    cout << "----\t\t----\t----\t----\n";
    cout << "Joe\t\t\t43\t\t7\t\t50\n";
    cout << "William\t\t50\t\t10\t\t49\n";
    cout << "Mary Sue\t50\t\t10\t\t49\n";
    return 0;
}
