#include <iostream>
using namespace std;
// Using do while loop, write a program that outputs the numbers
// 28,26,24,22,20,18,16,14,12 The end.
int main() {
    int myNumber = 28;
    do{
        cout<<myNumber<<" ";
        myNumber = myNumber - 2;
    } while(myNumber >= 12);
    cout<<"The end";
    return 0;

}
