#include <iostream>
using namespace std;

int main() {
    //Prompt the user for a number and print good if the number is between 14 & 72 or greater than 103.
    // Otherwise, print bad. Use the || operator in your if statement.
    int myNumber;
    cout<<"Enter your number ";
    cin>>myNumber;
    if(myNumber >= 14 && myNumber <= 72 || myNumber > 103){
        cout<<"GOOD";
    } else {
        cout<<"BAD";
    }
    return 0;
}
