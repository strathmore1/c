cmake_minimum_required(VERSION 3.26)
project(21_Lab6_9)

set(CMAKE_CXX_STANDARD 17)

add_executable(21_Lab6_9 main.cpp)
