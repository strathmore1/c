cmake_minimum_required(VERSION 3.26)
project(3_practise)

set(CMAKE_CXX_STANDARD 17)

add_executable(3_practise main.cpp)
