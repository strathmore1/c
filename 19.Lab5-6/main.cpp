#include <iostream>
using namespace std;

int main() {
    /*
     * Write a program which indicates whether a number entered by the user
     * is negative or positive
     * */
    double myNumber;
    cout<<"Enter a 'double' number ";
    cin>>myNumber;

    if(myNumber < 0){
        cout<<"Your number is negative number";
    } else {
        cout<<"Your number is a positive number";
    }
    return 0;
}
