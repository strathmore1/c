#include <iostream>
using namespace std;

int main() {
    /* Write a program which reads an integer from the user and decides
     * whether that integer is even or odd
     * */
    int integer;
    cout<<"Enter your number ";
    cin>>integer;

    if(integer % 2 == 0){
        cout<<"Your integer "<<integer<<" is even";
    } else {
        cout<<"Your integer "<<integer<<" is odd";
    }
    return 0;

    // Program to determine whether a year is leap year
}
