#include <iostream>
# include <cstdio>

using namespace std;

int main() {
    //  Fundamental Types
    // 1. Boolean -> this holds value true or false
    // has the bool as its type
    bool b;
    b = true;
    b = false;

    // 2. Character Type; this is used to represent a single character. These can be 'a', 'B'
    // The character type is exactly one byte.
    // character literals are enclosed in single quote in C++
    char c = 'a';
    char myChar = 'B';
    cout << "The value of character is : " << c << "\n";
    cout << "The value of character myChar is: " << myChar << "\n";

    printf("My character c is: %c\n", c);
    printf("My character myChar is: %c\n",myChar);
    printf("My boolean b is: %d\n",b);
    // returns 0 for false and 1 for true

    // 3. Integer types -> Used to store integral values (whole numbers) both negative and positive.
    int x = 1234;
    int y = 4567;
    cout << "The value of x is " << x << ", the value of y is " << y << "\n";

    // 4. Floating Points -> This is used to store decimal point numbers
    // they are of three types float, double, long double
    double pi = 3.142;
    double d = 213.546;
    double z = 567.908;
    double w = 0.15;

    printf("My double pi is %.2f\n",pi);
    printf("My double d is %.5f\n",d);
    printf("My double z is %.3f\n",z);
    printf("My double w is %.3f\n",w);

    //5. Type Void - this is  type with no values.
    // This is mostly used with functions of type void
    // These are functions that do not return any value
    // they can also be void pointer marked with void*

    // 6. Type Modifiers - Types can have modifiers. Some modifiers can be signed and unsigned.
    // signed modifiers means type can hold both positive and negative values.
    // unsigned modifiers can only hold positive values.
    // Type int is signed by default

    unsigned long int xy = 34858089459;

    // 7. Variables Declaration, Definition, and Initialization.
    // Introducing name into a current scope is called a declaration.
    // With declaration, we prepend the variable name with a type.
    // Examples of variable declaration
    char hisChar;
    int hisInt;
    double hisDouble;
    // we can declare multiple variables in the same line if they are of the same type.
    int herInt, theirInt;

    // Example of variable initialization
    char ab = 'a';
    int ba = 1234;
    double db = 456.789;

    // Exercises
    // Write a program with C++ which says C++ rocks
    cout << "C++ rocks \n";

    // Write a program that declares variables
    int gh;
    char myGh;
    double myDouble;

    // Write a program that defines variables
    gh = 233;
    myGh = 'G';
    myDouble = 23.456;

    return 0;
}
