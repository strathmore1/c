#include <iostream>
using namespace std;

//Write a program to output the numbers 7 6 5 4 3 2 1 0 -1 -2 -3 -4

int main() {
    int counter = 7;
    while(counter >= -4){
        cout<<counter<<" ";
        counter--;
    }
    return 0;
}
