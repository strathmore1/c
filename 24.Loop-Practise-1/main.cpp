#include <iostream>
using namespace std;

//Using the while loop, write a program that
// outputs the numbers 28,26,24, 22, 20,18,16,14,12

int main() {
    int number = 28;
    while(number > 11){
        cout<<number<<" ";
        number -= 2;
    }
    return 0;
}
