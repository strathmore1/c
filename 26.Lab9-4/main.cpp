#include <iostream>
using namespace  std;

// Write a program that asks the user for her or his age. Use a do-while loop to print
// “Happy Birthday!” for every year of the user’s age.

int main() {
    int userAge;
    int initialAge = 1;
    cout<<"Enter your age ";
    cin>>userAge;
    do{
        cout<<"Happy birthday \n";
        initialAge++;
    } while(initialAge <= userAge);
    return 0;
}
