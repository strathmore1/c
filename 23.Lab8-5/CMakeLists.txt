cmake_minimum_required(VERSION 3.26)
project(23_Lab8_5)

set(CMAKE_CXX_STANDARD 17)

add_executable(23_Lab8_5 main.cpp)
