#include <iostream>
using namespace std;

//Using the while loop, write a C++ program to output product of numbers between 1 and 5 (1 and 5 inclusive).
// Note that any number multiplied by zero will be zero

int main() {
    int counter = 1;
    int product = 1;

    while(counter <= 5){
        product *= counter;
        cout<<counter<<" ";
        counter ++;
    }
    cout<<" \n";
    cout<<"Product "<<product;

    return 0;
}
