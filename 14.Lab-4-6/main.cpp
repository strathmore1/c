#include <iostream>
using namespace std;

int main() {
    /*
     * Write a program that allows a user to input their year of birth
     * and current year and the program computes their age.
     * */

    int yearOfBirth;
    int currentYear;
    int yourAge;

    cout<<"What is your year of birth? ";
    cin>>yearOfBirth;
    cout<<"What is the current year? ";
    cin>>currentYear;
    yourAge = currentYear - yearOfBirth;
    cout<<"You are "<<yourAge<<" years old";
    return 0;
}
