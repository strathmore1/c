#include <iostream>
using namespace std;
int main() {
    // Write a program that accepts the name of a salesperson and
    // their monthly total sales and computes the commission to be earned based on the below table.
    string name;
    int monthlySales;
    int commissionDue;
    cout<<"Enter your name ";
    cin>>name;
    cout<<"Enter your monthly gross sales ";
    cin>>monthlySales;
    if(monthlySales >= 20000 && monthlySales <= 80000){
        commissionDue = 0.02 * monthlySales;
        cout<<"Name "<<name;
        cout<<"Monthly sales "<<monthlySales;
        cout<<"Commission Due "<<commissionDue;

    }else if(monthlySales >= 80001 && monthlySales <= 120000){
        commissionDue = 0.05 * monthlySales;
        cout<<"Name "<<name;
        cout<<"Monthly sales "<<monthlySales;
        cout<<"Commission Due "<<commissionDue;
    } else if(monthlySales >= 120001){
        commissionDue = 0.1 * monthlySales;
        cout<<"Name "<<name;
        cout<<"Monthly sales "<<monthlySales;
        cout<<"Commission Due "<<commissionDue;
    } else {
        cout<<"Wrong inputs";
    }
    return 0;
}
