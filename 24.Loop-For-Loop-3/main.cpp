#include <iostream>
using namespace std;
// Use for loop to display numbers from 73-415;
int main() {
    int sum = 0;
    for(int x = 73; x <= 415; x = x + 1){
        //cout<<x<<" ";
        sum = sum + x;
    }
    cout<<"The sum is "<<sum;
    return 0;
}
