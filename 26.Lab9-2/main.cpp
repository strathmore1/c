#include <iostream>
using namespace std;

// Using the for loop, write a program
// that outputs the numbers 12,14,16, 18, 20,22,24,26,28

int main() {
    for(int number = 12; number <= 28; number += 2){
        cout<<number<<", ";
    }
    return 0;
}
