#include <iostream>
using namespace  std;

int main() {
    // Write a program that prompts users to
    // input the age of 3 people and the
    // program determines the oldest among them

    int ageOne;
    int ageTwo;
    int ageThree;

    cout<<"Enter the first age ";
    cin>>ageOne;
    cout<<"Enter the second age ";
    cin>>ageTwo;
    cout<<"Enter the third age ";
    cin>>ageThree;

    if(ageOne > ageTwo && ageOne > ageThree){
        cout<<ageOne<<" is oldest";
    } else if(ageTwo > ageOne && ageTwo > ageThree){
        cout<<ageTwo<<" is oldest";
    } else if(ageThree > ageTwo && ageThree > ageOne){
        cout<<ageThree<<" is the oldest";
    } else {
        cout<<"They are age mates";
    }

    return 0;
}
