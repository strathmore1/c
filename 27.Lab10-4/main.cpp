#include <iostream>
using namespace std;
// Write a program that allows a user to input two integers.
// Using a function named MAX finds the largest of two integers.
void  max(int a, int b){
    int max = 0;
    if(a > b){
        max = a;
    } else if(b > a){
        max = b;
    }
    cout<<"The maximum integer is "<<max;
}

int main() {
    int firstInteger;
    int secondInteger;
    cout<<"Enter first integer ";
    cin>>firstInteger;
    cout<<"Enter second integer ";
    cin>>secondInteger;
    max(firstInteger,secondInteger);
    return 0;
}
