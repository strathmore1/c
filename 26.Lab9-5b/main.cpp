#include <iostream>
using namespace std;
// Write a C++ program using the for loop to find
// the sum of the integers 73 through 415 (both inclusive).
// Display the resulting sum.
int main() {
    int sum = 0;
    for(int number = 73; number <= 415; number++){
        sum += number;
    }
    cout<<"The sum of numbers between 73 and 415 is "<<sum;
    return 0;
}
