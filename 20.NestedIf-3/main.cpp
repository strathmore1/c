#include <iostream>
using namespace std;

int main() {
    // Initialize the variables
    int numb;
    int output;
    char S,s,C,c,response;

    cout<<"Enter your number ";
    cin>>numb;
    cout<<"Enter your character ";
    cin>>response;

    if(response == 'S' || response == 's'){
        output = numb *numb;
        cout<<output;
    } else if(response == 'C' || response == 'c'){
        output = numb * numb * numb;
        cout<<output;
    } else {
        cout<<"Error: Incorrect values";
    }

    return 0;
}
