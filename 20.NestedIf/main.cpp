#include <iostream>
using namespace std;

int main() {
    /*
     * Write a program that checks whether an integer input by a user is positive,
     * negative, or zero
     * */
    int number;
    cout<<"Enter your number ";
    cin>>number;

    if(number > 0){
        cout<<number<<" is positive";
    } else if(number < 0){
        cout<<number<<" is negative";
    } else {
        cout<<number<<" is zero";
    }
    return 0;
}
