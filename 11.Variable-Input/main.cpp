#include <iostream>

using namespace std;

int main() {
    // write a program that prompts a user to input surname and middle name initials
    // the program should output the surname

    string surname;
    char middleNameInitial;

    // we use prompts to input values
    // use cin -> c++ operator for input
    cout<<"Input your surname ";
    cin>>surname;

    cout << "Input middle name initial ";
    cin>>middleNameInitial;

    cout<<"Your surname is "<<middleNameInitial<<surname;

    return 0;
}
