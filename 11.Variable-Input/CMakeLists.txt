cmake_minimum_required(VERSION 3.26)
project(11_Variable_Input)

set(CMAKE_CXX_STANDARD 17)

add_executable(11_Variable_Input main.cpp)
