#include <iostream>
#include <string>

using namespace std;

int main() {
    // The university would like to equip lab of 35 sitter with imacs
    // program to compute the total cost to equip the lab if the cost of each imac is 282,999
    int numberOfSeats = 35;
    int imacPrice = 282999;
    int totalCost = numberOfSeats * imacPrice;
    cout << "The total cost to equip the lab is " << totalCost;
    return 0;
}
