#include <iostream>
using namespace std;

int main() {

    /*
     * Variables
     * It is a memory location to store values that will change
     It is  a data that will change
     Stored in RAM memory
     * Compute area of a rectangle using excel given L = 8 W =5
     *
     * STEPS
     * 1. Determine the number of variables required, length, width, and area
     * 2. Uniquely name the variables, e.g length, width and area. This is known as identifier
     * 3. Indicate the data type of identifier. Data can be numbers, fractions, string/letters/words,
     * 4. Assign variables to data types.
     * 5. Perform computations
     * 6. Output the results
     * */

    // Steps 1,2,3
    int len; //length
    int wid; //width
    int ar; // area

    // Step 4
    len = 8;
    wid = 5;

    // Step 5

    ar = len * wid;

    cout  << "The area is " << ar << "\n";

    /*
     * Naming Guidelines
     * - identifiers is a universal name.
     * - variables are placeholders of memory locations
     * - each variable needs an identifier that distinguishes from the other.
     * - identifiers can be letters of the alphabet
     * - can also be a combination of letters eg tbt, kjw, hbvs.
     * - contains numbers but should not begin with a digit eg w4, h9
     * - valid identifier should not have a space in between letters.
     * - identifier can be presented as firstname or first_name, firstName
     * - special characters that can be used as an identifier is the underscore (_). Others are invalid.
     * - C++ is case-sensitive, name and Name are different.
     * - identifiers should not be key words or reserved words.
     * - keywords are words that have special meaning already attached.
     *
     * */
    return 0;
}
