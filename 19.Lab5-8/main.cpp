#include <iostream>
using namespace std;

int main() {
    // A store uses the following policy to award discount.
    // If the customer purchases products costing more than ksh 30,000 she is given a discount of 10%,
    // otherwise she is given a discount of 2%.
    // Write a complete C++ program that allows a user to input the total value of purchases
    // then the program computes and outputs the discount a customer will receive.
    int total;
    float discount;
    cout<<"Enter your shopping totals ";
    cin>>total;

    if(total >= 30000){
        discount = 0.1 * total;
        cout<<"Your discount is "<<discount;
    } else {
        discount = 0.02 * total;
        cout<<"Your discount is "<<discount;
    }
    return 0;
}
