#include <iostream>
using namespace std;

int main() {
    // C++ is strongly typed language.
    // means it has int, char, string, bytes, bite as variable types

    // Initializing a Variable
    int the_answer = 25;
    int lucky_number = 50;
    return 0;
}
