#include <iostream>
using namespace std;
int main() {
    // Write a program that will prompt the user to enter a number and a character.
     // If the character is “S” the program will display the Square of the number;
     // if it is “C” it will display the Cube of the number.
    string myCharacter;
    int myNumber;
    cout<<"Enter your character 'S' or 'C' ";
    cin>>myCharacter;
    cout<<"Enter your number ";
    cin>>myNumber;
    if(myCharacter == "S"){
        cout<<"The square of your number is "<<myNumber * myNumber;
    } else if(myCharacter == "C"){
        cout<<"The cube of your number is "<<myNumber * myNumber * myNumber;
    }
    return 0;
}
