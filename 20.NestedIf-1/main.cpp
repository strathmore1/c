#include <iostream>
using namespace std;

int main() {
    /*
     * Write a program which grades students from A,B,C,Repeat
     * */

    int mark;
    cout<<"Enter your mark ";
    cin>>mark;

    if(mark >= 70 && mark <= 100){
        cout<<"A";
    } else if(mark >= 60 && mark <= 69){
        cout<<"B";
    } else if(mark >= 50 && mark <= 59){
        cout<<"C";
    } else if(mark >= 0 && mark <= 49){
        cout<<"Repeat";
    } else {
        cout<<"Error:Invalid Input";
    }

    return 0;
}
