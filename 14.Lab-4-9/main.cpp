#include <iostream>
using namespace std;

int main() {
    /*
     * Write a program that prompts a user to input their full name and the program outputs.
     * Good Evening <<full name>> , Welcome to your bank account .
     * Where full name is the users full name.
     * */
    string fullName;
    cout<<"Input your full name ";
    getline(cin,fullName);
    cout<<"Good evening "<<fullName<<" , Welcome to your bank account";
    return 0;
}
