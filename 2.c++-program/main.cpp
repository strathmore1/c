// Hello World program in C++
#include <iostream>
#include <cmath>
// Header file for importing pre compiled methods or procedures
// they come with  compile when it is installed
// I/O is input and output iostream manages input and output
//
using namespace std;
// namespace is a dictionary of items, types, functions and variables
// the namespace contains definition of cin,cout, vector, map e.t.c
// ; denotes the end of a statement. Statement performs specific tasks unless otherwise
// # is a directive to the pre-processer

/* Multiline comments
 * Tells compiler where the comments ends.
 * */

int main() {
    // int main() - the main function of your program
    //
    cout << "Hello, World!\n";
    cout <<"This is endline test \n";
    cout <<"This is a new line \n \n this is a new line\n";

    cout << "Item\t\tRevised price\n";
    cout << "Bread\t\t500\n";
    cout << "Milk\t\t100\n";
    cout << "\"Hello world \""; // quoting a new line

    return 0;
}
