#include <iostream>
using namespace std;

// Using the do while loop, write a program that outputs
// the numbers 12,14,16, 18, 20,22,24,26,28

int main() {

    int myNumber =  12;

    do{
        cout<<myNumber<<", ";
        myNumber += 2;
    } while(myNumber <= 28);
    return 0;
}
