cmake_minimum_required(VERSION 3.26)
project(26_Lab9_1)

set(CMAKE_CXX_STANDARD 17)

add_executable(26_Lab9_1 main.cpp)
