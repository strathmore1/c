#include <iostream>
using namespace std;

int main() {
    /*
     *Write a program which reads a year (integer)
     * from the user and decides whether that year is a leap year.
     * A year is a leap year if it is divisible by 4.
     * */

    int year;
    cout<<"Welcome to leap year checker\n";
    cout<<"Enter the year ";
    cin>>year;

    if(year % 4 == 0){
        if(year % 100 != 0) {
            cout << "The year " << year << " is a leap year";
        } else {
            cout<<"The year "<<year<<" is not a leap year";
        }
    } else {
        cout<<"The year "<<year<<" is not a leap year";
    }
    return 0;
}
