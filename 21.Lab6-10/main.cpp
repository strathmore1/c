#include <iostream>
using namespace std;
int main() {
    // Write a program that reads a date from the user in numeric form.
    // For example, June 1, 2019 would be entered as the three integers 6 1 2019.
    // Your program must then determine if the date is a “valid” date.
    string myDate;
    cout<<"Enter your date in the format (dd mm yyyy): ";
    getline(cin,myDate);
    string delimiter = " ";
    size_t pos = 0;
    string token;
    int day, month, year;
    pos = myDate.find(delimiter);
    token = myDate.substr(0,pos); // Extracting date
    day = stoi(token); // changing it to integer
    myDate.erase(0,pos+delimiter.length());
    // Check for the validity of date
    if(day >= 1 && day <= 31){
        cout<<"Your date is 'VALID' ";
    } else{
        cout<<"Your date is 'INVALID' ";
    }
    return 0;
}
