#include <iostream>
using namespace std;
// Write a program to compute the area and perimeter of a square.
// The program should ask the user to enter a double number corresponding to the side
// length of the square and display the area and perimeter of the square.
void calculateAreaAndPerimeter(double length){
    double area = length * length;
    double perimeter = 4 * length;
    cout<<"Area of the square is  "<<area<<"\n";
    cout<<"Perimeter of the square is "<<perimeter;
}

int main() {
    double length;
    cout<<"Enter the length of your square ";
    cin>>length;
    calculateAreaAndPerimeter(length);
    return 0;
}
