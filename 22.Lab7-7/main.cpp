#include <iostream>
using namespace std;
/* Rewrite the following if-statement as an equivalent switch-statement. You should
write a complete program. int Digit;char value;if (Digit == 6)value = 'A';
 else if ((Digit >= 0) && (Digit <= 2))value = 'C';
else if ((Digit == 4) || (Digit == 5)) value = 'B';
else value = 'D';
 * */
int main() {
    int Digit;
    char value;
    switch (Digit) {
        case 6:
            value = 'A';
            break;
        case 0 ... 2:
            value = 'C';
            break;
        case 4 ... 5:
            value = 'B';
            break;
        default:
            value = 'D';
    }
    return 0;
}
