#include <iostream>
# include <cmath>
using namespace std;

int add(int a, int b){
    int r;
    r = a + b;

    return pow(r,2);
    // names given to function variables are 'parameters'
    // a,b are params and contain data type followed by an identifier like any regular var.
    // add is the name of the function.
    // it must be unique in a program.
    // int is the return data type which is linked to return.
    // return is a statement that terminates the execution of a function and returns control to
    // the calling function.
    // the real numbers in a function are arguments.
    // params are variables arguments are the real values.
    //
}

int main() {
    // main is predefined function
    // it is the entry point of a program.
    // Calling the function in main
    //
    int myAddition = add(3,5);
    cout<<"The result of add() is "<<myAddition;
    return 0;
}
