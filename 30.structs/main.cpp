#include <iostream>
using namespace std;

int main() {

    // Struct
    // is a data structure to hold group of related variables in one place.
    // each variable is a member of the structure.
    // Create a struct using 'struct' key word

    struct {
        int myNum;
        string myString;
    } myStructure;

    // Assigning values to a struct
    myStructure.myNum = 50;
    myStructure.myString = "This is a test string";

    // Accessing Structure Members.
    // use the (.) notation to access members of a struct
    // myStructure.myNum

    cout<<myStructure.myNum<<"\n";
    cout<<myStructure.myString<<"\n";

    // One struct with multiple Variables
    struct {
        string brand;
        string model;
        int year;
    } CarOne,CarTwo;

    // Struct One
    CarOne.brand = "BMW";
    CarOne.model = "X1";
    CarOne.year = 2018;

    // StructTwo

    CarTwo.brand = "Toyota";
    CarTwo.model = "Crown";
    CarTwo.year = 2017;


    return 0;
}
