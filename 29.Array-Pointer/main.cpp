#include <iostream>
using namespace std;

int main() {

    int luckyNumbers[5] ={2,5,6,9,4};
    cout<<luckyNumbers; // returns the address of the first element of the array
    cout<<&luckyNumbers[0];

    // These two lines below do the same thing.
    // Both will return 5
    cout<<luckyNumbers[2]<<"\n";
    cout<<*(luckyNumbers+2)<<"\n";
    // add the reference of another two values to get the value at that point.

    for(int i = 0; i <= 4; i++){
        cout<<"Number is ";
        cin>>luckyNumbers[i];
    }

    for(int i = 0; i <= 4; i++){
        cout<<*(luckyNumbers+i) <<" ";
        // will return the values entered in the luckyNumbers array
        // accessing memory location that does not belong to the array will create errors
        //
    }


    system("paused>0");
    return 0;
}
