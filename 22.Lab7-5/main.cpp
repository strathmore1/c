#include <iostream>
using namespace std;
//The system is coded in a manner that if 100 is entered as
//input it indicates “normal speed range”, if 101 is entered it gives “over-speed
//warning” indicating that aircraft is over-speeding. If 88 is entered, it indicates “Low
//Oil Pressure” and if 187 is entered it indicates “Engine Failure”. Use switch
//statement to implement this logic.
int main() {
    int engineStatus;
    cout<<"Enter your engine status ";
    cin>>engineStatus;
    switch (engineStatus) {
        case 100:
            cout<<"Info: Normal speed range";
            break;
        case 101:
            cout<<"Warning: Over-speeding";
            break;
        case 88:
            cout<<"Waring: Low oil pressure";
            break;
        case 187:
            cout<<"Fatal: Engine failure";
            break;
        default:
            cout<<"Warning: Unknown operation";
    }
    return 0;
}
