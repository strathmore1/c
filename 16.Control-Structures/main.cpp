#include <iostream>
using namespace  std;

int main() {
    // CONTROL STRUCTURE - 'CONDITIONAL STRUCTURES'
    // statement that evaluates to true or false.
    // they are three constructs; 'if', 'if else', 'nested if'


    // 1. IF -> checks whether a condition is true or false.
    // only execute if the condition is true
    // syntax -> if(){}
    // if has one limitation -> only executes when the condition is fulfilled/true

    // 2. IF...ELSE
    // executes when the evaluation is both true or false
    // there is no need for repeating age < 20
    // they are key words
    // keywords: test condition -> that which evaluates to true or false
    //

    int age;

    cout<<"What is your age ";
    cin>>age;

    if(age >= 20){
        cout<<"You are eligible to vote ";
    } else {
        cout<<"You are not eligible to vote ";
    }



    return 0;
}
