#include <iostream>
using namespace std;

int main() {
    /*
     * Write a program that allows a user to input the radius
     * and height of a cylinder and the program computes the volume.
     *
     * */
    float pi = 3.142;
    float radius;
    float height;
    cout<<"What is the radius? ";
    cin>>radius;
    cout<<"What is the height? ";
    cin>>height;
    float volume = pi * radius * radius * height;
    cout<<"The volume of the cylinder is "<<volume;
    return 0;
}
