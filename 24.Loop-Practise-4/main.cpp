#include <iostream>
using namespace std;
// Using the while loop, write a program that outputs the statement “I am sorry”
// a number of times specified by the user

int main() {

    int myInput;
    int initialNumber = 1;
    cout<<"Enter your number ";
    cin>>myInput;

    while(myInput >= initialNumber){
        cout<<"Sorry \n";
        myInput --;
    }

    return 0;
}
