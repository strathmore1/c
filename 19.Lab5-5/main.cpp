#include <iostream>
using namespace std;

int main() {
    /*
     * Write a program that prompts a user to input the length and breadth of a polygon
     * and checks if it is a square or not.
     * */
    int length;
    int breadth;
    cout<<"Check if your polygon is square or not\n";
    cout<<"Enter the length of your polygon ";
    cin>>length;
    cout<<"Enter the breadth of your polygon ";
    cin>>breadth;

    if(length == breadth){
        cout<<"Your polygon is a square";
    } else {
        cout<<"Your polygon is not a square";
    }
    return 0;
}
