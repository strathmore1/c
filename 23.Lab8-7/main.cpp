#include <iostream>
using namespace std;
// You deposit 2 shillings today in your piggy bank, 4 shillings tomorrow, 8 shillings tomorrow but one, 16 shillings tomorrow but two.
// You keep on doubling the amount for a period of 6 consecutive days.
// Using the while loop, write a program that will determine the total amount of money you will have saved for the 6 days.
// [HINT: Total amount saved is 126]
int main() {
    int days = 6;
    int currentDay = 1;
    int savedAmount = 0;
    int currentDeposit = 2;
    while(currentDay <= days){
        savedAmount +=currentDeposit;
        currentDeposit *= 2;
        currentDay++;
    }
    cout<<"Amount saved for the six days is "<<savedAmount;
    return 0;
}
