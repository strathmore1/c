#include <iostream>
using namespace  std;

// Write a program to output the 10 9 8 6 5 4 3 2 1 Happy new year

int main() {
    int counter = 10;
    while(counter > 0){
        cout<<counter<<" ";
        counter --;
    }
    cout<<"Happy new year";
    return 0;
}
