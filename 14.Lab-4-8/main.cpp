#include <iostream>
using namespace std;

int main() {
    /* Write a program that asks the user to enter three numbers and prints the sum,
     * average and product, The output console should appear as below
     * */

    int firstNumber;
    int secondNumber;
    int thirdNumber;
    cout<<"Enter the first number ";
    cin>>firstNumber;
    cout<<"Enter the second number ";
    cin>>secondNumber;
    cout<<"Enter the third number ";
    cin>>thirdNumber;
    int result = firstNumber * secondNumber * thirdNumber;
    cout<<"The multiplication of your numbers is "<<result;
    return 0;
}
