#include <iostream>
using namespace std;

int main() {
    // program to compute profit made after selling a motor vehicle
    // program should input the buying price and selling price and compute the profit
    int buyingPrice;
    int sellingPrice;
    int profit;

    cout<<"What is the buying price of your car? ";
    cin>>buyingPrice;
    cout<<"What is the selling price of the car? ";
    cin>>sellingPrice;
    profit = sellingPrice - buyingPrice;

    cout<<"The profit is "<<profit;
    return 0;
}
