#include <iostream>
using namespace std;


struct Car {
    string brand;
    string model;
    int year;
};

struct User {
    int age;
    string name;
    int salary;
};



int main() {
    // Struct - used to group several related variables in one place.
    // Each variable is known as a member of the structure.
    // use struct key word to create the structure

    // 1. Creating struct
    struct {
        int myNum;
        string myString;

    } myStructure;

    // 2. Assigning members
    myStructure.myNum = 5;
    myStructure.myString ="This";


    // 3. Accessing structure members.
    cout<< myStructure.myNum <<"\n";
    cout<<myStructure.myString<<"\n";

    // 4. Named Structures
    // by giving a name to the structure, it can be a DATA TYPE.
    // you can create variables with this structure anywhere in the program at any time.
    // use struct myDataType {}

    User userOne; //
    userOne.age = 50;
    userOne.name = "James Bond";
    userOne.salary = 100000;

    cout<<userOne.name<<"\n";

    // Using struct as data type.
    Car bima;
    Car benz;

    bima.brand = "BMW";
    bima.model = "X1";
    bima.year = 1997;

    benz.brand = "Mercedes";
    benz.model = "G-Wagon";
    benz.year  = 2015;


    return 0;
}


