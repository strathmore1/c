#include <iostream>
using namespace std;

int getMin(int numbers[],int size){
    int min = numbers[0];

    for(int i = 1; i < size; i++){
        if(numbers[i] < min){
            min = numbers[i];
        }
    }

    return min;
}

int getMax(int numbers[],int size){
    int max = numbers[0];

    for(int i = 1; i < size; i++){
        if(numbers[i] > max){
            max = numbers[i];
        }
    }

    return max;
}

void getMinMax(int numbers[],int size,int* min, int* max){
    for(int i = 1; i < size; i++){
        if(numbers[i] < *min){
            *min = numbers[i];
            // dereferencing min with *min to get the value
        }
    }

    for(int i = 1; i < size; i++){
        if(numbers[i] > *max){
            *max = numbers[i];
            // dereferencing max with *max to get the value
        }
    }

    cout<<"Min number is "<<*min<<" \n";
    cout<<"Max number is "<<*max<<" \n";

}

int main() {
    // 1. Function return multiple types.
    // 2.

    int numbers [5] = {5,6,-2,30,7};

//    cout<<"Min is "<<getMin(numbers,5)<<"\n";
//    cout<<"Max is "<<getMax(numbers,5)<<"\n";

    int min = numbers[0];
    int max = numbers[0];

    getMinMax(numbers,5,&min,&max); // Passing parameter by reference. Passing address

    //cout<<"Min is "<<min<<"\n";
    //cout<<"Max is "<<max<<"\n";

    return 0;
}
