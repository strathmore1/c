#include <iostream>
using namespace  std;

int main() {
    // A program that inputs two inters and the program computes division
    int firstNumber;
    int secondNumber;
    cout<<"What is your first number? ";
    cin>>firstNumber;
    cout<<"What is your second number? ";
    cin>>secondNumber;
    int result = firstNumber/secondNumber;
    cout<<"The result of the division is "<<result;
    return 0;
}
