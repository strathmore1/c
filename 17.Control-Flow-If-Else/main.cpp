#include <iostream>
using namespace  std;

int main() {
   /*
    * Write a program which requests a user to input an integer.
    * If the integer is less than 6, the program multiplies the integer by 10
    * and outputs the result, otherwise the integer is multiplied by 5.
    * */

   int integer;
   int result;

   cout<<"Input your integer ";
   cin>>integer;

   if(integer < 6){
       result = integer * 10;
       cout<<"Your result is "<<result;
   } else {
       result = integer * 5;
       cout<<"Your result is "<<result;
   }
    return 0;
}
