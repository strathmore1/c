#include <iostream>
using namespace std;

// write a program that prompts user to input their PIN.
// the program verifies the pin and if the pin is correct, output 'Pin Verified'
// After 4 attempts the program will output 'Limit Expired' then exit.
// Note: Assume and initialize the correct pin in the program

int main() {

    int pin = 4321;
    bool flag = true;
    int attempts = 1;
    int inputPin;

    cout<<"Enter your pin ";
    cin>>inputPin;

    if(inputPin == pin){
        cout<<"Pin Verified ";
    } else if(inputPin != pin) {
        cout<<"Wrong Pin!";
        cout<<"Enter your pin ";
        cin>>inputPin;
        while(flag){
            attempts += 1;
            if(attempts <= 4){
                continue;
            } else {
                cout<<"Limit expired";
                flag = false;
            }
        }
    }

    return 0;
}
