#include <iostream>
using namespace std;

int main() {
    /*
     * Write a program that computes the simple interest
     * on a loan by allowing a user to input the principal,
     * interest rate and duration in years.
     * */

    float principal;
    float interest;
    float duration;

    cout<<"What loan amount would you like? ";
    cin>>principal;
    cout<<"What is the interest rate? ";
    cin>>interest;
    cout<<"What is the repayment period? ";
    cin>>duration;

    float simpleInterest = (principal * interest * duration)/100;
    cout<<"The simple interest is "<<simpleInterest;

    return 0;
}
