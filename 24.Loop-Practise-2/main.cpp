#include <iostream>
using namespace std;
// Using the while loop, write a program that outputs
// the numbers 28,26,24, 22, 20,18,16,14,12 The end
int main() {
    int myNumber = 28;
    while(myNumber > 11){
        cout<<myNumber<<", ";
        myNumber -= 2;
    }
    return 0;
}
