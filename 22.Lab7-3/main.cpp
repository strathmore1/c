#include <iostream>
using namespace  std;
//Write a program using the switch select structure that will prompt the user to enter a
//character. If the character is “S” the program will display the Square of a number 15;
//if it is “C” it will display the Cube of the same number.
int main() {
    char myChar;
    cout<<"Enter your character 'S' or 'C' ";
    cin>>myChar;
    switch(myChar){
        case 'S':
            cout<<"The square of 15 is "<<15 * 15;
            break;
        case 'C':
            cout<<"The cube of 15 is "<<15 * 15 * 15;
            break;
        default:
            cout<<"Err: Your character is not acceptable";
    }
    return 0;
}
