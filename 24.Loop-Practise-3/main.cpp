#include <iostream>
using namespace std;
// Using While loop, write a program to compute the sum of numbers 1and 5.

int main() {
    int myNumber = 0;
    int sumOfNumber = 0;
    while(myNumber <= 5){
        sumOfNumber += myNumber;
        myNumber ++;
    }
    cout<<"The total of numbers between 0 and 5 is "<<sumOfNumber;

    return 0;
}
