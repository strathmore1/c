#include <iostream>
using namespace  std;

int main() {
    /*
     * Write a program that initialiazes total fee for the semester as Ksh 200,000
     * The program prompts student to input student number, and amount of the fee paid in installments.
     * Finally, let the program compute the balance the student has
     * return <<Student | Number>> Your fee balance is <fee balance>
     * */

    int semesterFee = 200000;
    int studentNumber;
    int firstInstallment;
    int feeBalance;
    cout<<"Enter your student number ";
    cin>>studentNumber;
    cout<<"Enter your installment ";
    cin>>firstInstallment;
    feeBalance = semesterFee - firstInstallment;
    cout<<"Student "<<studentNumber<<" your fee balance is "<<feeBalance<<" Kenya shillings";

    return 0;
}
