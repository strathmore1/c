#include <iostream>
using namespace std;

int main() {

    // 4. Write a program to check whether an integer
    // input by a user is divisible by 5 and 11 or not.

    int myInput;
    cout<<"Enter your number ";
    cin>>myInput;

    if(myInput % 5 == 0 && myInput % 11 == 0){
        cout<<myInput<<" is divisible by 5 and 11";
    } else {
        cout<<myInput<<" is NOT divisible by 5 and 11";
    }
    return 0;
}
