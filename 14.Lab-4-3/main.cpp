#include <iostream>
using namespace  std;

int main() {
    /*
     * Write a simple program that allows a user input any
     * two integers and the program computes the modulus.
     * */

    int integerOne;
    int integerTwo;
    cout<<"What is  your first integer? ";
    cin>>integerOne;
    cout<<"What is your second integer? ";
    cin>>integerTwo;

    int result = integerOne % integerTwo;

    cout<<"The remainder of your division is "<<result;
    return 0;
}
