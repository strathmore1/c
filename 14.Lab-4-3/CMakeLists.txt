cmake_minimum_required(VERSION 3.26)
project(14_Lab_4_3)

set(CMAKE_CXX_STANDARD 17)

add_executable(14_Lab_4_3 main.cpp)
