#include <iostream>
using namespace std;
// Write a C++ program using the do while loop to find
// the sum of the integers 73 through 415 (both inclusive).
// Display the resulting sum.
int main() {

    int firstInteger = 73;
    int lastInteger = 415;
    int sum = 0;

    do{
        sum += firstInteger;
        firstInteger ++;
    } while (firstInteger <= lastInteger);
    cout<<"The sum of integers between 73 and 415 is "<<sum;
    return 0;
}
